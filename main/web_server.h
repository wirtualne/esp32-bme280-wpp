/*
 * web_server.h
 *
 *  Created on: 3 lis 2020
 *      Author: chris
 */

#ifndef MAIN_WEB_SERVER_H_
#define MAIN_WEB_SERVER_H_

#include "esp_http_server.h"

typedef enum {
	TIME,
	TEMP,
	PRES,
	HUMI,
	LAST_CMD,
} web_cmd_t;

httpd_handle_t start_webserver(void);
void stop_webserver(httpd_handle_t server);
void web_server_send_event(web_cmd_t i, uint64_t time_ms, float y);



#endif /* MAIN_WEB_SERVER_H_ */
