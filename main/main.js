/**
 * 
 */

var source = null;
var chartT;

function main(){
	window.onload = event;

	Highcharts.setOptions({ 
		time: {timezone: 'Europe/Warsaw' },
		global : {useUTC : false}
	});

	chartT = new Highcharts.Chart({
 		chart:{ 
			renderTo : 'chart1' },
		title: { 
			text: 'BME280 Sensor' },
	 	series: [{
			name: 'Temperature',
	    	data: []},{
			name: 'Pressure',
			yAxis: 1,
	    	data: []},{
			name: 'Humidity',
	    	data: []}
		],
		plotOptions: {
	    	line: { animation: false, dataLabels: { enabled: true }}
		},
	  	xAxis: { 
			type: 'datetime',
	    	dateTimeLabelFormats: { second: '%H:%M:%S' }
	  	},
	  	yAxis: [
		{
   			title: { text: 'Temperature [C]' }
		},
		{
   			title: { text: 'Pressure [hPa]' },
		    opposite: true
		},
		{
   			title: { text: 'Humidity [%]' }
		}]
	});	
}
 
function event() {
	if(typeof(EventSource) !== "undefined") {
		source = new EventSource("event");
		source.addEventListener('message', onMessage, false);
		source.addEventListener("open", function() {
    		console.log("Connection was opened.");
		}, false);
  	}
}

function onMessage(ssEvent) {
	var data = JSON.parse(ssEvent.data);
	chartParse(data);
}

function eventClose() {
	if (source.OPEN) {
		source.close();
		source = null;
	}
}

function eventReStart() {
	if (source == null) {
		event();
	}
}

function measure() {
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function() {
    	if (this.readyState == 4 && this.status == 200) {
        	var data = JSON.parse(this.responseText);
        	chartParse(data);
    	}
	};
	var query = '/val';
	xhr.open('GET', query, true);
	xhr.send(null);
}

function chartParse(data) {
	x = data.x;
	y = data.y;
	if(chartT.series[data.type-1].data.length > 40) {
		chartT.series[data.type-1].addPoint([x, y], true, true, true);
	} else {
		chartT.series[data.type-1].addPoint([x, y], true, false, true);
	}
}
